package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// CommandLineArguments ...
type CommandLineArguments struct {
	append     CmdArg
	directory  CmdArg
	filetype   CmdArg
	folder     CmdArg
	hasSuffix  CmdArg
	hidden     CmdArg
	ignore     CmdArg
	newName    CmdArg
	omitSuffix CmdArg
	prefix     CmdArg
	prepend    CmdArg
	recursive  CmdArg
	suffix     CmdArg
	until      CmdArg
}

// CmdArg ...
type CmdArg struct {
	name      string
	abbr      string
	seperator string
	value     []string
	valid     bool
}

// NewCmdArg ...
func NewCmdArg(name string, abbr string, seperator string, value []string, valid bool) *CmdArg {
	return &CmdArg{name, abbr, seperator, value, valid}
}

// File ...
type File struct {
	name      string
	isDir     bool
	modTime   time.Time
	directory string
	size      int64
}

// NewFile ...
func NewFile(name string, isDir bool, modTime time.Time, parent string, size int64) File {
	return File{name, isDir, modTime, parent, size}
}

func commandLineArgs() CommandLineArguments {
	defaultSeperator := ";"
	var cmdArg CommandLineArguments
	cmdArg.append = *NewCmdArg("--append", "-app", defaultSeperator, []string{}, false)
	cmdArg.directory = *NewCmdArg("--directory", "-d", defaultSeperator, []string{}, false)
	cmdArg.filetype = *NewCmdArg("--filetype", "-f", defaultSeperator, []string{}, false)
	cmdArg.folder = *NewCmdArg("--folder", "-fo", defaultSeperator, []string{}, false)
	cmdArg.hasSuffix = *NewCmdArg("--has-suffix", "-hs", defaultSeperator, []string{}, false)
	cmdArg.hidden = *NewCmdArg("--hidden", "-h", defaultSeperator, []string{"false"}, false)
	cmdArg.ignore = *NewCmdArg("--ignore", "-i", defaultSeperator, []string{}, true)
	cmdArg.newName = *NewCmdArg("--to", "-t", defaultSeperator, []string{}, false)
	cmdArg.omitSuffix = *NewCmdArg("--omit-suffix", "-os", defaultSeperator, []string{}, false)
	cmdArg.prefix = *NewCmdArg("--prefix", "-p", defaultSeperator, []string{}, false)
	cmdArg.prepend = *NewCmdArg("--prepend", "-pre", defaultSeperator, []string{}, false)
	cmdArg.recursive = *NewCmdArg("--recursive", "-r", defaultSeperator, []string{"false"}, false)
	cmdArg.suffix = *NewCmdArg("--suffix", "-s", defaultSeperator, []string{}, false)
	cmdArg.until = *NewCmdArg("--until", "-u", defaultSeperator, []string{}, false)

	return cmdArg
}

// Defaults to false
func toBool(str *string) bool {
	res, err := strconv.ParseBool(*str)

	if err != nil {
		return false
	}

	return res
}

func isNumber(str *string) bool {
	_, err := strconv.ParseInt(*str, 10, 32)

	return err == nil
}

// Defaults to false
func toInt(str *string) int {
	nr, _ := strconv.ParseInt(*str, 10, 32)
	return int(nr)
}

func contains(item *string, array []string) bool {
	for i := range array {
		if *item == array[i] {
			return true
		}
	}

	return false
}

func hasSuffix(item *string, array []string) bool {
	for i := range array {
		if strings.HasSuffix(*item, array[i]) {
			return true
		}
	}

	return false
}

func isValidFileName(name *string) bool {
	forbiddenChars := []string{"/"}

	return !contains(name, forbiddenChars[:])
}

func handleCommandLineArgs(args []string) CommandLineArguments {
	cmdArgs := commandLineArgs()

	for i := 0; i < len(args); i += 2 {
		if args[i] == cmdArgs.directory.name || args[i] == cmdArgs.directory.abbr {
			// Command line argument: Directory

			cmdArgs.directory.value = strings.Split(args[i+1], cmdArgs.directory.seperator)
			cmdArgs.directory.valid = len(cmdArgs.directory.value) > 0
		} else if args[i] == cmdArgs.recursive.name || args[i] == cmdArgs.recursive.abbr {
			// Command line argument: Recursive

			if i+1 < len(args) && !strings.HasPrefix(args[i+1], "-") {
				cmdArgs.recursive.value = []string{strconv.FormatBool(toBool(&args[i+1]))}
				cmdArgs.recursive.valid = toBool(&args[i+1])
			} else {
				cmdArgs.recursive.value = []string{"true"}
				cmdArgs.recursive.valid = true

				i--
			}
		} else if args[i] == cmdArgs.ignore.name || args[i] == cmdArgs.ignore.abbr {
			// Command line argument: Ignore

			cmdArgs.ignore.value = strings.Split(args[i+1], cmdArgs.ignore.seperator)
		} else if args[i] == cmdArgs.hidden.name || args[i] == cmdArgs.hidden.abbr {
			// Command line argument: Hidden

			if i+1 < len(args) && !strings.HasPrefix(args[i+1], "-") {
				cmdArgs.hidden.value = []string{strconv.FormatBool(toBool(&args[i+1]))}
				cmdArgs.hidden.valid = toBool(&args[i+1])
			} else {
				cmdArgs.hidden.value = []string{"true"}
				cmdArgs.hidden.valid = true

				i--
			}
		} else if args[i] == cmdArgs.folder.name || args[i] == cmdArgs.folder.abbr {
			// Command line argument: Folder

			if i+1 < len(args) && !strings.HasPrefix(args[i+1], "-") {
				cmdArgs.folder.value = []string{strconv.FormatBool(toBool(&args[i+1]))}
				cmdArgs.folder.valid = toBool(&args[i+1])
			} else {
				cmdArgs.folder.value = []string{"true"}
				cmdArgs.folder.valid = true

				i--
			}
		} else if args[i] == cmdArgs.newName.name || args[i] == cmdArgs.newName.abbr {
			// Command line argument: To

			cmdArgs.newName.value = strings.Split(args[i+1], cmdArgs.newName.seperator)
			if len(cmdArgs.newName.value) > 0 && isValidFileName(&args[i+1]) {
				cmdArgs.newName.valid = true
			}
		} else if args[i] == cmdArgs.hasSuffix.name || args[i] == cmdArgs.hasSuffix.abbr {
			// Command line argument: Has suffix

			cmdArgs.hasSuffix.value = strings.Split(args[i+1], cmdArgs.hasSuffix.seperator)
			cmdArgs.hasSuffix.valid = len(cmdArgs.hasSuffix.value) > 0
		} else if args[i] == cmdArgs.filetype.name || args[i] == cmdArgs.filetype.abbr {
			// Command line argument: Filetype

			cmdArgs.filetype.value = strings.Split(args[i+1], cmdArgs.filetype.seperator)
			cmdArgs.filetype.valid = len(cmdArgs.filetype.value) > 0
		} else if args[i] == cmdArgs.prefix.name || args[i] == cmdArgs.prefix.abbr {
			// Command line argument: Prefix

			cmdArgs.prefix.value = strings.Split(args[i+1], cmdArgs.prefix.seperator)
			cmdArgs.prefix.valid = len(cmdArgs.prefix.value) > 0
		} else if args[i] == cmdArgs.until.name || args[i] == cmdArgs.until.abbr {
			// Command line argument: Until

			cmdArgs.until.value = strings.Split(args[i+1], cmdArgs.until.seperator)
			cmdArgs.until.valid = len(cmdArgs.until.value) > 0
		} else if args[i] == cmdArgs.prepend.name || args[i] == cmdArgs.prepend.abbr {
			// Command line argument: Prepend

			cmdArgs.prepend.value = strings.Split(args[i+1], cmdArgs.prepend.seperator)
			cmdArgs.prepend.valid = len(cmdArgs.prepend.value) > 0
		} else if args[i] == cmdArgs.append.name || args[i] == cmdArgs.append.abbr {
			// Command line argument: Append

			cmdArgs.append.value = strings.Split(args[i+1], cmdArgs.append.seperator)
			cmdArgs.append.valid = len(cmdArgs.append.value) > 0
		} else if args[i] == cmdArgs.omitSuffix.name || args[i] == cmdArgs.omitSuffix.abbr {
			// Command line argument: Omit suffix

			cmdArgs.omitSuffix.value = strings.Split(args[i+1], cmdArgs.omitSuffix.seperator)
			cmdArgs.omitSuffix.valid = len(cmdArgs.omitSuffix.value) > 0
		} else if args[i] == cmdArgs.suffix.name || args[i] == cmdArgs.suffix.abbr {
			// Command line argument: Suffix

			cmdArgs.suffix.value = strings.Split(args[i+1], cmdArgs.suffix.seperator)
			cmdArgs.suffix.valid = len(cmdArgs.suffix.value) > 0
		} else {
			fmt.Println("Command not recognized.")
			os.Exit(1)
		}
	}

	return cmdArgs
}

func main() {
	argv := os.Args[1:]

	if len(argv) == 0 {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter arguments: ")
		in, _ := reader.ReadString('\n')
		argv = strings.Fields(in)
	}

	args := handleCommandLineArgs(argv[:])

	hasSuffix := args.hasSuffix.value
	ignoredDirs := args.ignore.value
	omitSuffix := args.omitSuffix.value
	prefix := args.prefix.value
	showHidden := args.hidden.valid
	showFolder := args.folder.valid
	suffix := args.suffix.value

	var directories []string
	if args.directory.valid {
		directories = args.directory.value
	} else {
		directories = []string{"."}
	}

	dirs := make(map[string]File)
	fileList := make(map[string][]File)

	for _, ele := range directories {
		if !showHidden && strings.HasPrefix(ele, ".") && ele != "." {
			continue
		}

		err := OpenDirectory(ele)
		if err != nil {
			fmt.Println("Can't open directory", ele)
			continue
		}

		dirs[ele] = NewFile(ele, true, time.Now(), "./", -1)
		if args.recursive.valid {
			dirs = getDirectories(ele, dirs, math.MaxInt32, showHidden, ignoredDirs)
		}

		for _, dir := range dirs {
			files := getFiles(dir, showHidden, showFolder, hasSuffix, omitSuffix)
			if len(files) > 0 {
				fileList[dir.name] = getFiles(dir, showHidden, showFolder, hasSuffix, omitSuffix)
			}
		}
	}

	for _, files := range fileList {
		var newName string
		var fileType string
		var until string
		var prepend string

		if args.filetype.valid {
			fileType = args.filetype.value[0]
		}

		if args.filetype.valid {
			fileType = args.filetype.value[0]
			if fileType[0:1] == "." {
				fileType = fileType[1:]
			}
		}

		if args.newName.valid {
			if args.newName.value[0] == "-f" || args.newName.value[0] == "--folder" {
				newName = files[0].directory
				newName = newName[strings.LastIndex(newName, "/")+1:]
			} else {
				newName = args.newName.value[0]
			}

			renameFiles(files[:], prefix, []bool{true, false, false, false, false, false, false}, fileType, newName, until, prepend)
		}

		if args.until.valid {
			renameFiles(files[:], prefix, []bool{false, false, false, true, false, false, false}, fileType, newName, args.until.value[0], prepend)
		}

		if args.prefix.valid {
			renameFiles(files[:], prefix, []bool{false, false, true, false, false, false, false}, fileType, newName, until, prepend)
		}

		if args.suffix.valid {
			renameFiles(files[:], suffix, []bool{false, false, false, false, false, false, true}, fileType, newName, until, prepend)
		}

		if args.filetype.valid {
			renameFiles(files[:], prefix, []bool{false, true, false, false, false, false, false}, fileType, newName, until, prepend)
		}

		if args.prepend.valid {
			renameFiles(files[:], prefix, []bool{false, false, false, false, true, false, false}, fileType, newName, until, args.prepend.value[0])
		}

		if args.append.valid {
			renameFiles(files[:], prefix, []bool{false, false, false, false, false, true, false}, fileType, newName, until, args.append.value[0])
		}
	}
}

// OpenDirectory ...
func OpenDirectory(dir string) error {
	_, err := ioutil.ReadDir(dir)

	return err
}

func renameFiles(files []File, prefix []string, options []bool, newFileType string, newName string, untilStr string, preApp string) {
	renameFile := options[0]
	changeFiletype := options[1]
	removePrefix := options[2]
	untilString := options[3]
	prependString := options[4]
	appendString := options[5]
	removeSuffix := options[6]
	pattern := newName + " \\([0-9]+\\)$"

	for i := range files {
		fileDirNames := getFileDirNames(files)
		ele := files[i]
		fileType := getFileType(ele.name)

		if untilString {
			if untilStr == "-n" || untilStr == "0-9" {
				newName = ele.name
				first := string(newName[0])
				for isNumber(&first) {
					newName = newName[1:]
				}
			} else {
				newName = ele.name[strings.Index(ele.name, untilStr)+len(untilStr):]
			}
		} else if !renameFile {
			newName = ele.name
		} else {
			matches, _ := regexp.MatchString(pattern, ele.name)
			if matches || newName == ele.name {
				continue
			}
		}

		if strings.HasSuffix(ele.name, newFileType) && changeFiletype {
			continue
		}

		suf := "." + fileType
		if changeFiletype {
			suf = "." + newFileType
		}

		if len(fileType) == 0 {
			if !changeFiletype {
				suf = ""
			}
		} else {
			if changeFiletype || removeSuffix {
				newName = ele.name[:len(ele.name)-len(fileType)-1]
			}
			if removePrefix || untilString || prependString || appendString {
				newName = newName[:len(newName)-len(fileType)-1]
			}
		}

		if removePrefix || removeSuffix {
			var pref string
			val := false

			for _, item := range prefix {
				if removePrefix {
					if strings.HasPrefix(ele.name, item) {
						pref = item
						val = true
						break
					}
				} else {
					if strings.HasSuffix(newName, item) {
						pref = item
						val = true
						break
					}
				}
			}

			if !val {
				continue
			}

			if removePrefix {
				newName = ele.name[len(pref):]
			} else {
				newName = newName[:len(newName)-len(pref)]
			}
		}

		if prependString {
			newName = preApp + newName
		} else if appendString {
			newName += preApp
		}

		wholeName := ele.directory + "/" + newName + suf
		if contains(&wholeName, fileDirNames[:]) {
			nr1 := getHighestCopyNumber(ele.directory+"/"+newName, fileDirNames[:])

			newName = changeCopyNumber(newName, strconv.FormatInt(int64(nr1+1), 10))
		}

		err := os.Rename(ele.directory+"/"+ele.name, ele.directory+"/"+newName+suf)
		files[i] = NewFile(newName+suf, ele.isDir, ele.modTime, ele.directory, ele.size)
		if err != nil {
			fmt.Println("ERROR: ", err)
		}
	}
}

func getFileDirNames(files []File) []string {
	var fileNames []string

	for _, item := range files {
		fileNames = append(fileNames, item.directory+"/"+item.name)
	}

	return fileNames
}

func getHighestCopyNumber(newName string, files []string) int {
	nr := 0

	for _, ele := range files {
		if strings.HasPrefix(ele, newName) {
			start := strings.LastIndex(ele, "(")
			end := strings.LastIndex(ele, ")")

			if start == -1 || end == -1 {
				continue
			}

			part := ele[start+1 : end]
			temp := toInt(&part)

			if temp > nr {
				nr = temp
			}
		}
	}

	return nr
}

func changeCopyNumber(fileName string, nr string) string {
	if strings.LastIndex(fileName, "(") != -1 {
		fileName = fileName[:strings.LastIndex(fileName, "(")-1]
	}

	fileName += " (" + nr + ")"

	return fileName
}

func max(nr1 int, nr2 int) int {
	if nr1 > nr2 {
		return nr1
	}

	return nr2
}

func getDirectories(dir string, subdirs map[string]File, depth int, showHidden bool, ignore []string) map[string]File {
	elements, _ := ioutil.ReadDir(dir)

	for _, item := range elements {
		name := item.Name()
		if item.IsDir() && !contains(&name, ignore[:]) {
			if !showHidden && strings.HasPrefix(item.Name(), ".") {
				continue
			}

			subdirs[dir+"/"+item.Name()] = NewFile(item.Name(), item.IsDir(), item.ModTime(), dir+"/", item.Size())

			if strings.Count(dir+"/"+item.Name(), "/") < depth {
				defer getDirectories(dir+"/"+item.Name(), subdirs, depth, showHidden, ignore)
			}
		}
	}

	return subdirs
}

func getFileType(fileName string) string {
	index := strings.LastIndex(fileName, ".")

	if index == -1 {
		return ""
	}

	return fileName[index+1:]
}

func getFiles(directory File, showHidden, renameFolder bool, suffix []string, omitSuffix []string) []File {
	elements, _ := ioutil.ReadDir(directory.directory + directory.name)
	var files []File

	for _, item := range elements {
		if !item.IsDir() || renameFolder {
			fileType := getFileType(item.Name())
			itN := item.Name()
			dotFT := "." + fileType
			if (!showHidden && strings.HasPrefix(item.Name(), ".") || (len(suffix) > 0 && !(contains(&fileType, suffix[:]) || contains(&dotFT, suffix[:])))) ||
				(len(omitSuffix) > 0 && hasSuffix(&itN, omitSuffix)) {
				continue
			}

			files = append(files, NewFile(item.Name(), item.IsDir(), item.ModTime(), directory.directory+"/"+directory.name, item.Size()))
		}
	}

	return files
}
